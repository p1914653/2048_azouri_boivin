import modele.Jeu;
import modele.Menu;
import vue_controleur.*;

public class Main {

    public static void main(String[] args) {
        //mainConsole();
        mainSwing();

    }

    public static void mainConsole() {
        Jeu jeu = new Jeu(4);
        Console2048 vue = new Console2048(jeu);
        jeu.addObserver(vue);

        vue.start();

    }

    public static void mainSwing() {
        Menu menu = new Menu();
        Menu2048 vuem = new Menu2048(menu);
        menu.addObserver(vuem);
        vuem.setVisible(true);

    }



}
