package vue_controleur;

import modele.EndGame;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class EndGame2048 extends JFrame implements Observer {
    private static final int WIDTH = 800;
    private static final int HEIGHT= 1000;

    private JLabel score;
    private JLabel time;
    private JLabel[][] scores;
    private JTextField pseudo;
    private JButton valider;
    private JButton passer;
    private EndGame eg;


    public EndGame2048(EndGame _eg) {
        eg = _eg;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        Container globalPane = new Container();
        globalPane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        JLabel info = new JLabel("Fin de partie, meilleurs scores:", SwingConstants.CENTER);
        JPanel scoreboard = new JPanel(new GridLayout(6,3));

        info.setFont(new Font("Serif", Font.PLAIN, 20));
        score = new JLabel(eg.getgScore(), SwingConstants.CENTER);
        time = new JLabel(eg.getgTime(), SwingConstants.CENTER);
        passer = new JButton("Ignorer");

        score.setFont(new Font("Serif", Font.PLAIN, 20));

        valider = new JButton("Valider");

        pseudo = new JTextField("Pseudo",15);

        Border border = BorderFactory.createLineBorder(Color.darkGray, 3);
        scores = new JLabel[5][3];
        for(int i =0; i<3; i++){
            scores[0][i] = new JLabel();
            scores[0][i].setFont(new Font("Serif", Font.PLAIN, 20));
            scores[0][i].setBorder(border);
            scores[0][i].setHorizontalAlignment(SwingConstants.CENTER);

            scoreboard.add(scores[0][i]);
        }

        scores[0][0].setText("Temps");
        scores[0][1].setText("Score");
        scores[0][2].setText("Joueur");

        for (int i = 0; i < 5; i++) {

            scores[i][0] = new JLabel();
            scores[i][0].setFont(new Font("Serif", Font.PLAIN, 20));
            scores[i][0].setBorder(border);
            scores[i][0].setHorizontalAlignment(SwingConstants.CENTER);
            scores[i][0].setText(eg.getsTime(i));

            scores[i][1] = new JLabel();
            scores[i][1].setFont(new Font("Serif", Font.PLAIN, 20));
            scores[i][1].setBorder(border);
            scores[i][1].setHorizontalAlignment(SwingConstants.CENTER);
            scores[i][1].setText(eg.getsScore(i));

            scores[i][2] = new JLabel();
            scores[i][2].setFont(new Font("Serif", Font.PLAIN, 20));
            scores[i][2].setBorder(border);
            scores[i][2].setHorizontalAlignment(SwingConstants.CENTER);
            scores[i][2].setText(eg.getsPseudo(i));


            scoreboard.add(scores[i][0]);
            scoreboard.add(scores[i][1]);
            scoreboard.add(scores[i][2]);
        }


        c.weighty =1;
        c.gridy=0;
        c.gridx=1;
        globalPane.add(info, c);

        c.gridy=1;
        c.gridx=1;
        globalPane.add(scoreboard, c);


        c.weighty =1.5;
        c.gridy=2;
        c.gridx=0;
        globalPane.add(time, c);

        c.gridx = 1;
        globalPane.add(score, c);

        if(Integer.parseInt(eg.getgScore()) > Integer.parseInt(eg.getsScore(4))) {
                    c.gridx = 2;
                    globalPane.add(pseudo, c);

                    c.weighty = 1;
                    c.gridy = 3;
                    c.gridx = 1;
                    globalPane.add(valider, c);

        }

        c.gridx =2;
        globalPane.add(passer, c);


        setContentPane(globalPane);
        addEventListener();
        rafraichir();
    }




    /**
     * Correspond à la fonctionnalité de Vue : affiche les données du modèle
     */
    private void rafraichir()  {

        SwingUtilities.invokeLater(new Runnable() { // demande au processus graphique de réaliser le traitement
            @Override
            public void run() {

            }
        });
    }

    /**
     * Correspond à la fonctionnalité de Contrôleur : écoute les évènements, et déclenche des traitements sur le modèle
     */
    private void addEventListener() {
        passer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eg.sortir();
            }
        });
        valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!pseudo.getText().equals(null)) {
                    eg.valider(pseudo.getText());
                    eg.sortir();
                }
            }
        });
    }


    @Override
    public void update(Observable o, Object arg) {
        rafraichir();
    }
}