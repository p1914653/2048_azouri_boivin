package vue_controleur;

import modele.Case;
import modele.Jeu;
import modele.Direction;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class Swing2048 extends JFrame implements Observer {
    private static final int PIXEL_PER_SQUARE = 150;
    // tableau de cases : i, j -> case graphique
    private JLabel[][] tabC;

    private JLabel score;
    private JButton revert;
    private Jeu jeu;


    public Swing2048(Jeu _jeu) {
        jeu = _jeu;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(jeu.getSize() * PIXEL_PER_SQUARE, (jeu.getSize()+1) * PIXEL_PER_SQUARE);
        tabC = new JLabel[jeu.getSize()][jeu.getSize()];

        Container globalPane = new Container();

        globalPane.setLayout(new BorderLayout());
        JPanel contentPane = new JPanel(new GridLayout(jeu.getSize(), jeu.getSize()));
        JPanel scorePane = new JPanel(new BorderLayout());
        JPanel northPane = new JPanel(new BorderLayout());
        score = new JLabel("Hello");
        score.setFont(new Font("Serif", Font.PLAIN, 20));
        Dimension dim2 = new Dimension(PIXEL_PER_SQUARE* jeu.getSize(),PIXEL_PER_SQUARE/2);
        score.setPreferredSize(dim2);

        Dimension dim = new Dimension(PIXEL_PER_SQUARE* jeu.getSize(),PIXEL_PER_SQUARE);
        scorePane.setPreferredSize(dim);
        score.setBorder(BorderFactory.createLineBorder(Color.darkGray, 5));
        score.setHorizontalAlignment(SwingConstants.CENTER);

        scorePane.add(score,BorderLayout.NORTH);

        revert = new JButton("Revert");
        northPane.add(revert,BorderLayout.EAST );
        northPane.add(scorePane, BorderLayout.CENTER);

        for (int i = 0; i < jeu.getSize(); i++) {
            for (int j = 0; j < jeu.getSize(); j++) {
                Border border = BorderFactory.createLineBorder(Color.darkGray, 5);
                tabC[i][j] = new JLabel();
                tabC[i][j].setFont(new Font("Serif", Font.PLAIN, 20));
                tabC[i][j].setBorder(border);
                tabC[i][j].setHorizontalAlignment(SwingConstants.CENTER);


                contentPane.add(tabC[i][j]);

            }
        }
        revert.setFocusable(false);
        globalPane.add(contentPane, BorderLayout.CENTER);
        globalPane.add(northPane, BorderLayout.NORTH);
        setContentPane(globalPane);
        ajouterEcouteurClavier();
        ajouterEcouteurSouris();
        rafraichir();

    }




    /**
     * Correspond à la fonctionnalité de Vue : affiche les données du modèle
     */
    private void rafraichir()  {

        SwingUtilities.invokeLater(new Runnable() { // demande au processus graphique de réaliser le traitement
            @Override
            public void run() {
                for (int i = 0; i < jeu.getSize(); i++) {
                    for (int j = 0; j < jeu.getSize(); j++) {
                        Case c = jeu.get(i, j);

                        if (c == null) {

                            tabC[i][j].setText("");
                            tabC[i][j].setOpaque(false);

                        } else {
                            tabC[i][j].setText(c.getValeur() + "");
                            tabC[i][j].setOpaque(true);
                            switch (c.getValeur()){
                                case 2:
                                    Color c2 = new Color(238, 228, 218);
                                    tabC[i][j].setBackground(c2);
                                    break;
                                case 4:
                                    Color c4 = new Color(237, 224, 200);
                                    tabC[i][j].setBackground(c4);
                                    break;
                                case 8:
                                Color c8 = new Color(242, 177, 121);
                                    tabC[i][j].setBackground(c8);
                                    break;
                                case 16:
                                Color c16 = new Color(245, 149, 99);
                                    tabC[i][j].setBackground(c16);
                                    break; 
                                case 32:
                                Color c32 = new Color(246, 124, 95);
                                    tabC[i][j].setBackground(c32);
                                    break;
                                case 64:
                                Color c64 = new Color(246, 94, 59);
                                    tabC[i][j].setBackground(c64);
                                    break;
                                case 128:
                                Color c128 = new Color(237, 207, 114);
                                    tabC[i][j].setBackground(c128);
                                    break;
                                case 256:
                                Color c256 = new Color(240, 224, 80);
                                    tabC[i][j].setBackground(c256);
                                    break;
                                case 512:
                                Color c512 = new Color(240, 224, 16);
                                    tabC[i][j].setBackground(c512);
                                    break; 
                                case 1024:
                                Color c1024 = new Color(240, 208, 0);
                                    tabC[i][j].setBackground(c1024);
                                    break;
                                case 2048:
                                    Color c2048 = new Color(240, 192, 0);
                                    tabC[i][j].setBackground(c2048);
                                    break;
                                case 4096:
                                    Color c4096 = new Color(0,0,255);
                                    tabC[i][j].setBackground(c4096);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                score.setText("Score : " + jeu.getScore());
            }

        });
    }

    /**
     * Correspond à la fonctionnalité de Contrôleur : écoute les évènements, et déclenche des traitements sur le modèle
     */
    private void ajouterEcouteurClavier() {
        addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {  // on regarde quelle touche a été pressée
                    case KeyEvent.VK_LEFT:
                        jeu.action(Direction.gauche);
                        break;
                    case KeyEvent.VK_RIGHT:
                        jeu.action(Direction.droite);
                        break;
                    case KeyEvent.VK_DOWN:
                        jeu.action(Direction.bas);
                        break;
                    case KeyEvent.VK_UP:
                        jeu.action(Direction.haut);
                        break;
                    case KeyEvent.VK_BACK_SPACE:
                        try {
                            jeu.revert();
                        } catch (IOException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                        break;
                }
                rafraichir();
                if(jeu.checkEnd()){
                    terminer();
                }
            }
        });
    }

    public void ajouterEcouteurSouris(){

        revert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    jeu.revert();
                    rafraichir();
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    @Override
    public void update(Observable o, Object arg) {
        rafraichir();
    }

    public void terminer(){this.setVisible(false);}
}