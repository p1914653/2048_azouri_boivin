package vue_controleur;


import modele.Menu;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;

public class Menu2048 extends JFrame implements Observer {
    private static final int WIDTH = 600;
    private static final int HEIGHT= 600;

    private JLabel size;
    private JButton plus;
    private JButton minus;
    private JButton valider;
    private JButton resume;
    private Menu menu;


    public Menu2048(Menu _menu) {
        menu = _menu;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        Container globalPane = new Container();
        globalPane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        JLabel info = new JLabel("Choisissez votre taille:", SwingConstants.CENTER);

        info.setFont(new Font("Serif", Font.PLAIN, 20));
        size = new JLabel("Hello", SwingConstants.CENTER);

        size.setFont(new Font("Serif", Font.PLAIN, 20));
        plus = new JButton("+");

        minus = new JButton("-");

        valider = new JButton("Valider");

        resume = new JButton("Reprendre partie précédente");


        c.weighty =1;
        c.gridy=0;
        c.gridx=1;
        globalPane.add(info, c);


        c.weighty =1.5;
        c.gridy=1;
        c.gridx=0;
        globalPane.add(minus, c);

        c.gridx = 1;
        globalPane.add(size, c);

        c.gridx=2;
        globalPane.add(plus, c);

        c.weighty =1;
        c.gridy =2;
        c.gridx =1;
        globalPane.add(valider,c);

        c.gridy = 3;
        c.gridx = 1;
        globalPane.add(resume, c);


        setContentPane(globalPane);
        addEventListener();
        rafraichir();
        menu.setSize(4);
    }




    /**
     * Correspond à la fonctionnalité de Vue : affiche les données du modèle
     */
    private void rafraichir()  {

        SwingUtilities.invokeLater(new Runnable() { // demande au processus graphique de réaliser le traitement
            @Override
            public void run() {
                size.setText(""+menu.getSize());
            }
        });
    }

    /**
     * Correspond à la fonctionnalité de Contrôleur : écoute les évènements, et déclenche des traitements sur le modèle
     */
    private void addEventListener() {
        plus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menu.setSize(menu.getSize()+1);
                rafraichir();
            }
        });
        minus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menu.setSize(menu.getSize()-1);
                rafraichir();
            }
        });
        valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menu.valider();
                terminer();
            }
        });
        resume.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menu.resume();
                terminer();
            }
        });
    }


    @Override
    public void update(Observable o, Object arg) {
        rafraichir();
    }

    public void terminer(){
        this.setVisible(false);
    }
}

