package modele;

import vue_controleur.EndGame2048;
import vue_controleur.Swing2048;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.sound.sampled.SourceDataLine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.SocketPermission;
import java.security.Policy;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Jeu extends Observable {

    private Case[][] tabCases;
    private int score;
    private HashMap<Case,Point> map;
    private static Random rnd = new Random();
    private static boolean hasMoved;
    private String debut;

    private static boolean hasFused;

    /**
     * Constructeur de la classe Jeu
     * @param size
     */
    public Jeu(int size) {
        tabCases = new Case[size][size];
        map = new HashMap<Case, Point>();
        score = 0;
        rnd();
        hasFused = false;
        try {
            File myObj = new File("data\\gameData.txt");
            if (myObj.createNewFile()) {
                System.out.println("Fichier Créé: " + myObj.getName());
            } 
        } catch (IOException e) {
            System.out.println("Une erreur dans la création du fichier est apparue");
            e.printStackTrace();
        }
        try {
            FileWriter myWriter = new FileWriter("data\\gameData.txt");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
            debut = dtf.format(LocalDateTime.now());
            myWriter.write(debut);
            myWriter.write(System.lineSeparator());
            myWriter.write(String.valueOf(size));
            myWriter.write(System.lineSeparator());
            myWriter.write("|");
            myWriter.close();
          } catch (IOException e) {
            System.out.println("Une erreur dans l'écriture du fichier gameData est apparue");
            e.printStackTrace();
          }
    }

    /**
    * Constructeur de la classe Jeu avec reprendrePartie
    * @param sizeS
    */
    public Jeu(String sizeS){
        int size = Integer.parseInt(sizeS); 
        tabCases = new Case[size][size];
        map = new HashMap<Case, Point>();
        score = 0;
        rnd();
        hasFused = false;
        reprendrePartie();
    }
    
    /**
    * getter de score
    */
    public int getScore(){
        return score;
    }

    /**
    * Fait le calcul du score et le met à jour à chaque coup
    * @param val
    */
    public void updateScore(int val){
        int value = val;
        double resultat;
        double coeff =0;
        while(value !=1){
            value = value/2;
            coeff ++;
        }
        resultat = (coeff-1) *(Math.pow(2, coeff));
        score +=resultat;
    }

    public int getSize() {
        return tabCases.length;
    }

    public Case get(int i, int j){
        return tabCases[i][j];
    }

    /**
     * Récupère la case dans la direction de la case en paramètre
     * @param d Direction
     * @param c Case
     * @return Case
     */
    public Case getCase(Direction d, Case c) {
        Point p = map.get(c);
        int px = p.x;
        int py = p.y;
        switch (d) {
            case gauche:
                if(py != 0){
                 py -= 1;}
                break;
            case droite:
                if(py != this.getSize()-1){
                py += 1;}
                break;
            case haut:
                if(px!=0){
                px -= 1;}
                break;
            case bas:
                if(px != this.getSize()-1){
                px += 1;}
                break;
            default:
                break;
        }
        return tabCases[px][py];
    }

    /**
     * Fait une action pour une direction choisie en paramètre, appel la méthode fusion pour fusionner les cases
     * et la methode move pour le déplacement des cases
     * Appelle EndGame si on ne peut plus bouger de cases
     * @param d
     */
    public void action(Direction d) {
        if(hasMoved){
            writeLastMove();
        }
        
        hasMoved =false;
        hasFused =false;
        for(int i = 0; i<this.getSize(); i++){
            for(int j=0; j<this.getSize(); j++){
                Case temp = this.get(i,j);
                if( temp!= null){temp.setFused(false);}
            }
        }
        
        switch (d) {
            case haut: {
                for (int j = 0; j <this.getSize(); j++) {
                    for (int i = 0; i <this.getSize(); i++) {
                        Case c =this.get(i, j);
                        if (c != null) {
                            c.move(d);
                        }
                    }
                }
                break;
            }
            case gauche: {
                for (int i = 0; i <this.getSize(); i++) {
                    for (int j = 0; j <this.getSize(); j++) {
                        Case c =this.get(i, j);
                        if (c != null) {
                            c.move(d);
                        }
                    }
                }
                break;
            }
            case bas: {
                for (int j = this.getSize()-1; j >= 0; j--) {
                    for (int i = this.getSize()-1; i >= 0; i--) {
                        Case c =this.get(i, j);
                        if (c != null) {
                            c.move(d);
                        }
                    }
                }
                break;
            }
            case droite: {
                for (int i = this.getSize()-1; i >= 0; i--) {
                    for (int j = this.getSize()-1; j >= 0; j--) {
                        Case c =this.get(i, j);
                        if (c != null) {
                            c.move(d);
                        }
                    }
                }
                break;
            }
            default:
                break;
        }
        int indice = 0;
        while(indice <16 && !hasFused){
            try {
                if (this.get(indice / this.getSize(), indice % 4).getFused()) {
                    hasFused = true;
                }
            }
            catch(Exception e){
            }
            indice ++;
        }
        
        if(hasMoved) {
            int pif = rnd.nextInt(getSize()*getSize());
            int x = pif / this.getSize();
            int y = pif % this.getSize();
            int rand = rnd.nextInt(2);
            while (tabCases[x][y] != null) {
                pif = rnd.nextInt(getSize()*getSize());
                x = pif / this.getSize();
                y = pif % this.getSize();
            }
            switch (rand) {
                case 0:
                    tabCases[x][y] = new Case(2, this);
                    map.put(tabCases[x][y], new Point(x, y));
                    break;
                case 1:
                    tabCases[x][y] = new Case(4, this);
                    map.put(tabCases[x][y], new Point(x, y));
                    score = score+4;
                    break;
            }
        }

        if (checkEnd()) {
            Date time = new Date(debut);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
            String end =dtf.format(LocalDateTime.now());
            Date fin = new Date(end);
            long duree = Math.abs(fin.getTime() - time.getTime());
            time = new Date(duree);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String temps = dateFormat.format(time.getTime());
            EndGame eg = new EndGame(temps, ""+score);
            EndGame2048 vue = new EndGame2048(eg);
            eg.addObserver(vue);
            vue.setVisible(true);
        }
    }

    /**
     * Fait le déplacement pour une case en fonction d'une direction
     * @param d
     * @param c
     */
    public void move(Direction d, Case c){
        Point ref = map.get(c);
        Point p = new Point(ref.x, ref.y);
        switch (d) {
            case gauche:
                if(p.y != 0) {
                    p.y -=1;
                    map.replace(c, p);
                    tabCases[p.x][p.y] = tabCases[p.x][p.y+1];
                    tabCases[p.x][p.y+1] = null;
                    c.move(d);
                    hasMoved=true;
                }
                break;
            case droite:
                if(p.y != this.getSize()-1) {
                    p.y += 1;
                    map.replace(c, p);
                    tabCases[p.x][p.y] = tabCases[p.x][p.y-1];
                    tabCases[p.x][p.y-1] = null;
                    c.move(d);
                    hasMoved=true;
                }
                break;
            case haut:
                if(p.x!= 0) {
                    p.x -= 1;
                    map.replace(c, p);
                    tabCases[p.x][p.y] = tabCases[p.x+1][p.y];
                    tabCases[p.x+1][p.y] = null;
                    c.move(d);
                    hasMoved=true;
                }
                break;
            case bas:
                if(p.x != this.getSize()-1) {
                    p.x += 1;
                    map.replace(c, p);
                    tabCases[p.x][p.y] = tabCases[p.x-1][p.y];
                    tabCases[p.x-1][p.y] = null;
                    c.move(d);
                    hasMoved=true;
                }
                break;
            default:
                break;
        }
    }

    /**
     * Fait la fusion pour une case en fonction d'une direction
     * @param d
     * @param c
     */
    public void fuse(Direction d, Case c){
        Point ref = map.get(c);
        int x = ref.x;
        int y = ref.y;
        switch(d) {
            case haut:
                if(x!=0) {
                    Case voisin = this.getCase(d, c);
                    voisin.setValeur(c.getValeur() * 2);
                    this.updateScore(voisin.getValeur());
                    voisin.setFused(true);
                    map.remove(c);
                    tabCases[x][y] = null;
                    hasMoved=true;
                }
                break;
            case droite:
                if(y!=this.getSize()-1) {
                    Case voisin = this.getCase(d, c);
                    voisin.setValeur(c.getValeur() * 2);
                    this.updateScore(voisin.getValeur());
                    voisin.setFused(true);
                    map.remove(c);
                    tabCases[x][y] = null;
                    hasMoved=true;
                }
                break;
            case bas:
                if(x!=this.getSize()-1) {
                    Case voisin = this.getCase(d, c);
                    voisin.setValeur(c.getValeur() * 2);
                    this.updateScore(voisin.getValeur());
                    voisin.setFused(true);
                    map.remove(c);
                    tabCases[x][y] = null;
                    hasMoved=true;
                }
                break;
            case gauche:
                if(y!=0) {
                    Case voisin = this.getCase(d, c);
                    voisin.setValeur(c.getValeur() * 2);
                    this.updateScore(voisin.getValeur());
                    voisin.setFused(true);
                    map.remove(c);
                    tabCases[x][y] = null;
                    hasMoved=true;
                }
                break;
        }
    }

    /**
     * Ecrit la dernière ligne jouée dans le fichier gameData avec le score
     */
    public void writeLastMove(){
        try {
            FileWriter myWriter = new FileWriter("data\\gameData.txt", true);
            myWriter.write(System.lineSeparator());
            for(int i = 0; i<this.getSize(); i++)
                for(int j=0; j<this.getSize(); j++){
                    if (tabCases[i][j] != null) myWriter.write(String.valueOf(tabCases[i][j].getValeur()) + " ");
                    else myWriter.write("null" + " ");
                }
            myWriter.write(String.valueOf(this.getScore()));
            myWriter.close();
          } catch (IOException e) {
            System.out.println("Une erreur dans l'écriture du fichier gameData est apparue.");
            e.printStackTrace();
          }
    }

    /**
     * Récupère la dernière ligne du fichier gameData
     * @return String
     */
    public String lastLineGameData() {
        try {
            File myObj = new File("data\\gameData.txt");
            Scanner myReader = new Scanner(myObj);
            String data = "";
            debut = myReader.nextLine();
            while (myReader.hasNextLine()) {
                String temp = myReader.nextLine();
                if (!temp.equals("")) data = temp;
            }
            myReader.close();
            if (data.equals("|")) return null;
            return data;

        } catch (FileNotFoundException e) {
            System.out.println("Le fichier n'existe pas");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retourne d'un coup en arrière grâce à la fonction lastLineGameData
     * supprime l'ancienne dernière ligne
     * @throws IOException
     */
    public void revert() throws IOException{
        String lastGamePos = lastLineGameData();
        if(!(lastGamePos == null)){
            String[] sepGamePos = lastGamePos.split(" ");
            map.clear();
            for(int i = 0; i<this.getSize(); i++)
                for(int j=0; j<this.getSize(); j++){
                    if(!sepGamePos[i*this.getSize()+j].equals("null")){
                        tabCases[i][j] = new Case(Integer.parseInt(sepGamePos[i*this.getSize()+j]), this);
                        map.put(tabCases[i][j], new Point(i, j));
                    }
                    else tabCases[i][j] = null;
                }
            score = Integer.parseInt(sepGamePos[sepGamePos.length-1]);
            RandomAccessFile f = new RandomAccessFile("data\\gameData.txt", "rw");
            long length = f.length() - 1;
            byte b;
            do {                     
                length -= 1;
                f.seek(length);
                b = f.readByte();
            } while(b != 10 && length > 20);
            f.setLength(length);
            f.close();
        }
    }

    /**
     * Crée un processus pour ajouter des cases de manière aléatoire
     */
    public void rnd() {
        Jeu ceci = this;
        new Thread() { // permet de libérer le processus graphique ou de la console
            public void run() {
                int r;
                for (int i = 0; i < tabCases.length; i++) {
                    for (int j = 0; j < tabCases.length; j++) {
                        r = rnd.nextInt(3);
                        Point p = new Point(i,j);

                        switch (r) {
                            case 0:
                                tabCases[i][j] = null;
                                break;
                            case 1:
                                tabCases[i][j] = new Case(2,ceci);
                                map.put(tabCases[i][j], p);
                                break;
                            case 2:
                                tabCases[i][j] = new Case(4,ceci);
                                map.put(tabCases[i][j], p);
                                score +=4;
                                break;
                        }
                    }
                }
            }

        }.start();

        setChanged();
        notifyObservers();


    }

    /**
     * Reprend le jeu enregistré dans le fichier gameData
     */
    public void reprendrePartie(){
        String lastGamePos = lastLineGameData();
        if(!(lastGamePos == null)){
            String[] sepGamePos = lastGamePos.split(" ");
            map.clear();
            for(int i = 0; i<this.getSize(); i++)
                for(int j=0; j<this.getSize(); j++){
                    if(!sepGamePos[i*this.getSize()+j].equals("null")){
                        tabCases[i][j] = new Case(Integer.parseInt(sepGamePos[i*this.getSize()+j]), this);
                        map.put(tabCases[i][j], new Point(i, j));
                    }
                    else tabCases[i][j] = null;
                }
            score = Integer.parseInt(sepGamePos[sepGamePos.length-1]);
        }
    }

    /**
     * Test si on ne peut plus bouger et appel la fin du jeu dans ce cas là
     * @return
     */
    public boolean checkEnd(){
        for(int i =0; i<this.getSize(); i++){
            for(int j =0; j<this.getSize(); j++){
                if(this.tabCases[i][j] == null){
                    return false;
                }
            }
        }
        for(int i =0; i<this.getSize(); i++) {
            for (int j = 0; j < this.getSize(); j++) {
                if (i != this.getSize()-1) {
                    if (tabCases[i + 1][j].getValeur() == tabCases[i][j].getValeur()) {
                        return false;
                    }
                }
                if (j != this.getSize()-1) {
                    if (tabCases[i][j + 1].getValeur() == tabCases[i][j].getValeur()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

}
