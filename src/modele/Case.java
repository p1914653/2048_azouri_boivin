package modele;

public class Case {
    private int valeur;
    private Jeu j;
    private boolean fused;

    public Case(int _valeur, Jeu jeu) {
        valeur = _valeur;
        fused = false;
        j = jeu;
    }

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur)
    {
        this.valeur = valeur;
    }

    public boolean getFused() {
        return fused;
    }

    public void setFused(boolean f){
        fused=f;
    }

    /**
     * Appel move puis fuse si la case n'est pas null et si elle n'a pas déjà fused et si elle a même valeur que la case
     * dans la direction passé en paramètre
     * @param d
     */
    public void move(Direction d){
        Case v = j.getCase(d,this);
        if(v==null){
            j.move(d,this);
        }
        else if(v.getValeur() == this.getValeur() && !v.equals(this) && !v.getFused()){
            j.fuse(d,this);
        }
    }

}
