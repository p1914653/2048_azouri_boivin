package modele;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observable;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class EndGame extends Observable{
    private ArrayList<LinkedList<String>> scoreboard;
    private String gTime;
    private String gScore;
    private String gPseudo;

    /**
     * constructeur de la classe EndGame
     * @param _gTime temps de la partie terminée
     * @param _gScore score de la partie terminée
     */
    public EndGame( String _gTime, String _gScore){
        gTime = _gTime;
        gScore= _gScore;
        scoreboard = new ArrayList<LinkedList<String>>();
        try {
            File myObj = new File("data\\scoreData.txt");
            if (myObj.createNewFile()) {
                System.out.println("Fichier Créé: " + myObj.getName());
                //mettre 5 lignes à 0
                try {
                    PrintWriter writer = new PrintWriter("data\\scoreData.txt", "UTF-8");
                    for(int i =0; i<5; i++){
                        writer.println("00:00:00&0&AAA");
                    }
                    writer.close();
                }
                catch(Exception e){
                    System.out.println("Problème d'écriture dans le fichier de scores");
                    e.printStackTrace();
                }
            }
            Scanner scnr = new Scanner(myObj);
            int indice =0;
            while(scnr.hasNextLine()){
                String line = scnr.nextLine();
                String[] words =line.split("&");
                LinkedList<String> tampon = new LinkedList<String>();
                for(int i = 0; i<3; i++) {
                    tampon.add(words[i]);
                }
                scoreboard.add(tampon);
                indice++;
            }
                scnr.close();
        } catch (IOException e) {
            System.out.println("Une erreur dans la création du fichier est apparue");
            e.printStackTrace();
        }


    }

    /**
     * Accesseur du score de la partie
     * @return score de la partie
     */
    public String getgScore(){//fonction qui renvoie le score de la partie (utilisé dans la vue)
        return gScore;
    }

    /**
     * Accesseur du Temps de la partie terminée
     * @return le temps de la partie
     */
    public String getgTime(){//fonction qui renvoie le temps de laa partie(utilisé dans la vue)
        System.out.println(gTime);
        String retour = gTime.split(" ")[1];
        System.out.println(retour);
        String tampon = Integer.parseInt(retour.split(":")[0]) -1+ ":" +retour.split(":")[1]+":"+retour.split(":")[2];
        return tampon;
    }

    /**
     * Accesseur de temps dans le tableau des meilleurs scores
     * @param ind indice du temps recherché
     * @return le temps de partie
     */
    public String getsTime(int ind){//fonction qui renvoie le temps d'une partie dans les meilleurs scores, celle d'indice ind
        return scoreboard.get(ind).get(0);
    }

    /**
     * Accesseur du score dans le tableau des meilleurs scores
     * @param ind indice du score recherché
     * @return Score de la partie
     */
    public String getsScore(int ind){
        return scoreboard.get(ind).get(1);
    }

    /**
     * Accesseur du pseudo des Meilleurs scores
     * @param ind indice du pseudo recherché
     * @return Pseudo du score
     */
    public String getsPseudo(int ind){
        return scoreboard.get(ind).get(2);
    }

    /**
     * Fonction de fin
     */
    public void sortir(){
        System.exit(0);
    }


    /**
     * Fonction d'inscription du score dans le tableau des meilleurs scores
     * @param pseudo pseudo du joueur à inscrire
     */
    public void valider(String pseudo){
        gPseudo = pseudo;
        if(gPseudo.contains("&")){gPseudo = gPseudo.replace("&", "");}
        // placer les infos dans l'arraylist puis update
        int ind = 5;
        do{
            ind --;
        }while(Integer.parseInt(scoreboard.get(ind).get(1)) < Integer.parseInt(gScore) && ind >0);

        LinkedList<String> tampon = new LinkedList<String>();
        tampon.add(getgTime());
        tampon.add(gScore);
        tampon.add(gPseudo);

        scoreboard.add(ind,tampon);
        scoreboard.remove(scoreboard.get(5));
        updateDoc();
    }

    /**
     * Met à jour le document de Hall Of Fame
     */
    public void updateDoc(){
        //coller l'array list dans le doc
        try {
            PrintWriter writer = new PrintWriter(new FileOutputStream("data\\scoreData.txt",false));
            for(LinkedList<String> ligne : scoreboard){
                writer.println(ligne.get(0)+"&"+ligne.get(1)+"&"+ligne.get(2));
            }
            writer.close();
        }
        catch(Exception e){
            System.out.println("Problème d'écriture dans le fichier de scores");
            e.printStackTrace();
        }
    }
}
