package modele;

import vue_controleur.Swing2048;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Observable;
import java.util.Scanner;

public class Menu extends Observable {
    private final int maxsize =6;
    private final int minsize = 3;
    private int size;

    /**
     * Constructeur du Menu, avec une taille à 4 par défaut
     */
    public void Menu(){
        size=4;
    }

    /**
     * Accesseur de la taille du jeu paramétrée
     * @return la taille paramétrée
     */
    public int getSize(){
        return size;
    }

    /**
     * Mise à jour du paramétrage de la taille
     * @param nsize taille paramétrée
     */
    public void setSize(int nsize){
        if(nsize>= minsize && nsize<=maxsize) {
            size = nsize;
        }
    }

    /**
     * Fonction mettant à jour la taille du jeu
     */
    public void valider(){
        size = this.getSize();
        Jeu jeu = new Jeu(size);
        Swing2048 vue = new Swing2048(jeu);
        jeu.addObserver(vue);
        vue.setVisible(true);
    }

    /**
     * fonction permettant de relancer la dernière partie en cours
     */
    public void resume(){
        File myObj = new File("data\\gameData.txt");
        if(myObj.exists() && !myObj.isDirectory()) {
            try {
                Scanner myReader = new Scanner(myObj);
                String date = myReader.nextLine();
                String sizeS = myReader.nextLine();
                Jeu jeu = new Jeu(sizeS);
                Swing2048 vue = new Swing2048(jeu);
                jeu.addObserver(vue);
                vue.setVisible(true);
            } catch (FileNotFoundException e) {
                System.out.println("Erreur dans la lecture du fichier");
                e.printStackTrace();
            } 
        }
    }
}
